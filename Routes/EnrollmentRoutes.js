var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var enrollmentctrl = require('../Controller/Enrollment');
router.post("/enrollment",authentication.auth,enrollmentctrl.enrollment);
router.post("",authentication.auth,enrollmentctrl.enrollment);
module.exports = router