var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var CompanyCtrl = require('../Controller/company');
router.get("/company", CompanyCtrl.CompanyAll);
module.exports = router
