var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var UserCtrl = require('../Controller/User');

router.get("/User", UserCtrl.UserAll);
router.get("/User/:username", UserCtrl.UserByUserName);
module.exports = router