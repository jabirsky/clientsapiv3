var connectdb = require('../Helper/dbconnect')
const jwt = require('jsonwebtoken')
require('dotenv').config()


var token = (req,res) => {
    let ClientCode = req.query.ClientCode || ''
    let KeyToken = req.query.KeyToken || ''
    console.log(ClientCode)
    console.log(KeyToken)
    connectdb.query(`get_clientbyclientcodeandkeytoken @clientcode='${ClientCode}', @keytoken='${KeyToken}'`,{ type: connectdb.QueryTypes.SELECT})
        .then(
            result => {
                console.log(result[0]);
                if (result[0] === undefined)
                {
                    let resultfailed = {
                        ResponseCode : 1,
                        ResponseDescription : 'Invalid ClientCode or KeyToken',
                        Token : ''
                    }
                        res.send(resultfailed)
                        return
                }
                let token = jwt.sign({
                        ClientCode: ClientCode
                        },
                        process.env.SECRET_KEY)
                        res.set('Authorization', 'Bearer ' + token)
                        let resultsucces = {
                            ResponseCode : 0,
                            ResponseDescription : 'Success',
                            Token : token
                        }
                        res.send(resultsucces)
                
            }
        ).catch(
            err => {
                res.send(err)
            }
        )
}


module.exports = { token } 