var rolesMod = require('../Model/Roles')


var rolesAll = async(req, res) => {
    try{
        console.log('panggil rolesAll');
        var getRoles = await rolesMod.findAll();
        res.send(getRoles);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}

var RolesByName = async(req, res) => {
    try{
        console.log(req);
        var getRoles = await rolesMod.findAll(
            {
                where: {
                    Name : req.params.rolename
                  }
            }
        );
        res.send(getRoles);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}


module.exports =  { rolesAll,RolesByName }