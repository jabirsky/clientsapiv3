var express = require('express');
var router = express.Router();
var cancelpaypendctrl = require('../Controller/cancelpaypend');
var authentication = require('../Helper/AuthMiddleware')
router.get("", authentication.auth, cancelpaypendctrl.cancelpaypend);
router.get("/cancelpaypend",authentication.auth,cancelpaypendctrl.cancelpaypend);
router.post("/cancelpaypendbybatchnumber",authentication.auth,cancelpaypendctrl.cancelpaypendbybatchnumber);

module.exports = router ;