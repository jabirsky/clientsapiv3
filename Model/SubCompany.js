var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var CompanyMod =require('../Model/company')

class subcompany extends Sequelize.Model {}
subcompany.init({
  SubCompanyId: {type:Sequelize.INTEGER(10),primaryKey: true, autoIncrement: true},
  SubCompanyName: Sequelize.STRING,
  CompanyID: {
    type: Sequelize.INTEGER,
    references: {
        // This is a reference to another model
        model: CompanyMod,
        
        // This is the column name of the referenced model
        key: 'CoyID',
      }
  }
}, { 
    sequelize:connectdb, 
    modelName: 'SubCompany' ,
    freezeTableName: true,
    timestamps: false
});



module.exports =  subcompany 