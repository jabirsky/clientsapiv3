var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var ClientMod =require('../Model/client')
var SubCompanyMod =require('../Model/SubCompany')

class company extends Sequelize.Model {}
company.init({
  CoyID: {type:Sequelize.INTEGER(10),primaryKey: true, autoIncrement: true},
  CoyName: Sequelize.STRING,
  ClientID: {
    type: Sequelize.INTEGER,
    references: {
        // This is a reference to another model
        model: ClientMod,
        
        // This is the column name of the referenced model
        key: 'ClientID',
      }
  }
}, { 
    sequelize:connectdb, 
    modelName: 'tb_company' ,
    freezeTableName: true,
    timestamps: false
});
// company.associate = function(models) {
//     company.hasOne(models.ClientMod, {foreignKey: 'ClientID',sourceKey: 'ClientID'})
// }
//client.hasMany(CompanyMod,{ foreignKey: 'ClientID' })
//company.belongsTo(ClientMod,{ foreignKey: 'ClientID' })

company.hasMany(SubCompanyMod,{foreignKey:'CompanyID'})
SubCompanyMod.belongsTo(company,{foreignKey:'CompanyID'})

module.exports =  company 