var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')

class Roles extends Sequelize.Model {}
Roles.init({
  Id: {type:Sequelize.STRING,primaryKey: true,allowNull:false},
  Name: Sequelize.STRING
}, { 
    sequelize:connectdb, 
    modelName: 'AspNetRoles' ,
    freezeTableName: true,
    timestamps: false
});

module.exports =  Roles 