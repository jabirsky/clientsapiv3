var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')
var UserRolesMod =require('../Model/UserRoles')
var UserClientMod =require('../Model/UserClient')
var ClientMod =require('../Model/client')
var ClinicMod =require('../Model/Clinic')
var CompanyMod =require('../Model/company')
var UserCompanyMod =require('../Model/UserCompany')
var UserMatrixMod =require('../Model/UserMatrix')


class User extends Sequelize.Model {}
User.init({
  Id: {type:Sequelize.STRING,primaryKey: true,allowNull:false},
  Email: Sequelize.STRING,
  EmailConfirmed: Sequelize.STRING,
  PhoneNumber: Sequelize.STRING,
  UserName: Sequelize.STRING
}, { 
    sequelize:connectdb, 
    modelName: 'AspNetUsers' ,
    freezeTableName: true,
    timestamps: false
});

User.hasOne(UserRolesMod,{foreignKey : 'UserId', as : 'UserRoles'})
UserRolesMod.belongsTo(User,{foreignKey : 'UserId'})
User.belongsToMany(ClientMod,{
    as: 'Clients',
    through : {
        model : UserClientMod
    },
    foreignKey : 'UserId'
})
ClientMod.belongsToMany(User,{
    through : {
        model : UserClientMod
    },
    foreignKey : 'ClientId'
})

User.belongsToMany(CompanyMod,{
    as: 'Companies',
    through : {
        model : UserCompanyMod
    },
    foreignKey : 'UserId'
})
CompanyMod.belongsToMany(User,{
    through : {
        model : UserCompanyMod
    },
    foreignKey : 'CompanyID'
})

User.belongsTo(ClinicMod,{foreignKey:'ProviderId'})
ClinicMod.hasMany(User,{foreignKey:'ProviderId'})

User.hasMany(UserMatrixMod,{foreignKey:'UserId'})
//UserMatrixMod.hasOne(User,{foreignKey:'UserId'})

//UserClientMod.belongsTo(User,{foreignKey : 'UserId'})

module.exports =  User 




