var express = require('express');
var router = express.Router();
var tokenctrl = require('../Controller/Token');
router.get("/token",tokenctrl.token);

module.exports = router