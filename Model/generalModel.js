var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')
var sequelize = connectdb

class employee extends Sequelize.Model {}
employee.init({
    employeename : Sequelize.STRING,
  }, { 
    sequelize:connectdb, 
    modelName: 'dw_employee' ,
    underscored: true,
    freezeTableName: true,
    timestamps: false
});

class claim extends Sequelize.Model {}
claim.init({
  claimnumber : Sequelize.STRING,
  UNID: {
    type: Sequelize.INTEGER,
    references: {
        // This is a reference to another model
        model: employee,
        
        // This is the column name of the referenced model
        key: 'Id',
      }
  }
  }, { 
    sequelize:connectdb, 
    modelName: 'dw_claim' ,
    freezeTableName: true,
    timestamps: false
});

employee.hasMany(claim,{foreignKey: 'UNID'})
var emptesaja = claim.belongsTo(employee,{foreignKey: 'UNID'})


class Document extends Sequelize.Model {}
Document.init({
  author: Sequelize.STRING
}, { sequelize, modelName: 'document', timestamps: false  });

class Version extends Sequelize.Model {}
Version.init({
  namever: Sequelize.STRING
}, { 
    sequelize, 
    modelName: 'version',
    freezeTableName: true,
    timestamps: false 
});


class vcompany extends Sequelize.Model {}
vcompany.init({
  CoyID : { type: Sequelize.INTEGER,primaryKey:true,field: 'CoyID' },
  ClientName : { type: Sequelize.INTEGER,primaryKey:true,field: 'ClientName' },
  CoyName : { type: Sequelize.INTEGER,primaryKey:true,field: 'CoyName' },
  }, { 
    sequelize:connectdb, 
    freezeTableName: true,
    modelName: 'v_company' ,
    underscored: true,
    
    timestamps: false
});



Document.hasMany(Version); // This adds documentId attribute to version
Document.belongsTo(Version, {
  as: 'Current',
  foreignKey: 'currentVersionId',
  constraints: false
}); // This adds currentVersionId attribute to document

module.exports =  { employee, claim, emptesaja, Document,  Version, vcompany} 



