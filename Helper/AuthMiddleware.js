const jwt = require('jsonwebtoken')
require('dotenv').config()


var auth = (req,res,next) => {
    console.log(req.body);
    console.log(req.body.ClientCode)
    if (!req.get('Authorization')) {
        return res.status(401).send({ResponseCode: 98, ResponseDescription: 'You dont have access!'})
    }
    const token = req.get('Authorization').replace("Bearer ","")
    console.log(token)
    let ClientCode = req.body.ClientCode ? req.body.ClientCode : req.query.ClientCode
    jwt.verify(token, process.env.SECRET_KEY, (err, decrypt) => {
        if (!err) {
                console.log(ClientCode);
                console.log(decrypt);
                if (String(decrypt.ClientCode) === String(ClientCode)) {
                next()
            } 
            else {
                res.status(402).send({ResponseCode:96,ResponseDescription: 'Token is not authorized for client code: ' + String(ClientCode)})
            } 
        } else {
            console.log('error', err)
            res.status(401).send({ResponseCode:97, ResponseDescription: 'Token expired/invalid'})
        }
    })
}

module.exports = { auth }