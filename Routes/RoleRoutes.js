var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var RolesCtrl = require('../Controller/Roles');

router.get("/Roles", RolesCtrl.rolesAll);
router.get("/Roles/:rolename", RolesCtrl.RolesByName);
module.exports = router