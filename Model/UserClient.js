var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var ClientMod =require('../Model/client')

class UserClient extends Sequelize.Model {}
UserClient.init({
  UserId: {type:Sequelize.STRING,allowNull:false},
  ClientId: Sequelize.INTEGER
}, { 
    sequelize:connectdb, 
    modelName: 'AspNetUserClient' ,
    freezeTableName: true,
    timestamps: false
});


ClientMod.hasOne(UserClient,{foreignKey : 'ClientId', as : 'Client'})
UserClient.belongsTo(ClientMod,{foreignKey : 'ClientId', as : 'Client'})

module.exports =  UserClient 
