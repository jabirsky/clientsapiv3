var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var ClaimsCtrl = require('../Controller/ClaimItem');
router.get("/claimitems/Claimid/:ClaimId", ClaimsCtrl.claimItemsByClaim);
router.get("/claimitems", ClaimsCtrl.claimItemsAll);
module.exports = router

