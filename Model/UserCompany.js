var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var ClientMod =require('../Model/client')

class UserCompany extends Sequelize.Model {}
UserCompany.init({
  CompanywiseUserMappingID: {type:Sequelize.INTEGER,primaryKey:true ,allowNull:false},
  UserId: {type:Sequelize.STRING,allowNull:false},
  CompanyID: Sequelize.INTEGER
}, { 
    sequelize:connectdb, 
    modelName: 'CompanyWiseUserMapping' ,
    freezeTableName: true,
    timestamps: false
});

module.exports =  UserCompany 