var generalmod = require('../Model/generalModel')
var financeprocessingtypeMod = require('../Model/financeprocessingtype')
var companyMod = require('../Model/company')


var ClaimAll = async(req, res) => {
    try{
        console.log('panggil employeall');
        var createemp = await generalmod.claim.sync();
        var getClaim = await generalmod.claim.findAll({
            include :[{
                model: generalmod.employee,
                include:
                [{
                    model : generalmod.claim
                }]
            }]
        });
        res.send(getClaim);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}


module.exports =  { ClaimAll }