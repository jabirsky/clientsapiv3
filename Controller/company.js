var clientMod = require('../Model/client')
var financeprocessingtypeMod = require('../Model/financeprocessingtype')
var companyMod = require('../Model/company')


var CompanyAll = async(req, res) => {
    try{
        console.log('panggil companyall');
        var getCompany = await companyMod.findAll(
         {
            include :[{
                model: clientMod,
                include : [{
                    model: financeprocessingtypeMod
                }]
            }]
         }
        );
        res.send(getCompany);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}


module.exports =  { CompanyAll }