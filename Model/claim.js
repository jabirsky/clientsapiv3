var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')

class Claim extends Sequelize.Model {}
Claim.init({
  ROCNo: {type:Sequelize.STRING,primaryKey: true,allowNull:false},
  ROCDate: Sequelize.DATE,
  UNID: Sequelize.INTEGER,
  benefitId : Sequelize.UUIDV4,
}, { 
    sequelize:connectdb, 
    modelName: 'tb_nprocppo' ,
    freezeTableName: true,
    timestamps: false
});

module.exports =  Claim 