var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var FinanceProcessingTypeCtrl = require('../Controller/FinanceProcessingType');
router.get("/fpt", FinanceProcessingTypeCtrl.financeprocessingtypeAll);
module.exports = router

