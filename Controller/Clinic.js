var clinicMod = require('../Model/Clinic')


var clinicAll = async(req, res) => {
    try{
        console.log('panggil Clinic all');
        var getClinic = await clinicMod.findAll();
        res.send(getClinic);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}

module.exports =  { clinicAll }