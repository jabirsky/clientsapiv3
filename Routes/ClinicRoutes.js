var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var ClinicCtrl = require('../Controller/Clinic');
router.get("/clinic", ClinicCtrl.clinicAll);
module.exports = router
