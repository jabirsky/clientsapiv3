var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var RolesMod =require('../Model/Roles')

class UserRoles extends Sequelize.Model {}
UserRoles.init({
  UserId: {type:Sequelize.STRING,primaryKey: true,allowNull:false},
  RoleId: Sequelize.STRING
}, { 
    sequelize:connectdb, 
    modelName: 'AspNetUserRoles' ,
    freezeTableName: true,
    timestamps: false
});


RolesMod.hasOne(UserRoles,{foreignKey : 'RoleId', as : 'Roles'})
UserRoles.belongsTo(RolesMod,{foreignKey : 'RoleId', as : 'Roles'})

module.exports =  UserRoles 
