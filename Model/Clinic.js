var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')

class Clinics extends Sequelize.Model {}
Clinics.init({
  ClinicID: {type:Sequelize.STRING,primaryKey: true,allowNull:false},
  ClinicName: Sequelize.STRING,
  ClinicCode: Sequelize.STRING,
  MerchantID : Sequelize.STRING,
}, { 
    sequelize:connectdb, 
    modelName: 'tb_clinic' ,
    freezeTableName: true,
    timestamps: false
});

module.exports =  Clinics 