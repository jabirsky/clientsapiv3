var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var ClientCtrl = require('../Controller/client');
router.get("/client", ClientCtrl.ClientAll);
router.post("/client", ClientCtrl.InsertClient);
router.put("/client", ClientCtrl.UpdateClient);
router.delete("/client", ClientCtrl.DeleteClient);
module.exports = router

