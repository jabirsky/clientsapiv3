var financeprocessingtype = require('../Model/financeprocessingtype')
var ClientMod = require('../Model/client')

var financeprocessingtypeAll = async(req, res) => {
    try{
        var getClient = await financeprocessingtype.findAll(
            {
                include:[{
                    model:ClientMod
                }]
            }
        );
        res.send(getClient);
    }
    catch(e)
    {
        res.send(e);
    }
}


module.exports =  { financeprocessingtypeAll }