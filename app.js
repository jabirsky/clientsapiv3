var express = require('express')
const paypendroutes = require('./Routes/PaypendRoutes')
const tokenroutes = require('./Routes/TokenRoutes')
const enrollmentroutes = require('./Routes/EnrollmentRoutes')
const claimroutes = require('./Routes/ClaimRoutes')
const clientroutes = require('./Routes/ClientRoutes')
const financeProcessingTypeRoutes = require('./Routes/financeprocessingtypeRoutes')
const companyRoutes = require('./Routes/CompanyRoutes')
const subcompanyRoutes = require('./Routes/SubCompanyRoutes')
const employeeRoutes = require('./Routes/EmployeeRoutes')
const claimDevRoutes = require('./Routes/claimdevRoutes')
const tesRoutes = require('./Routes/tesRoutes')
const userRoutes = require('./Routes/UserRoutes')
const rolesRoutes = require('./Routes/RoleRoutes')
const userRolesRoutes = require('./Routes/UserRolesRoutes')
const clinicRoutes = require('./Routes/ClinicRoutes')
const claimsRoutes = require('./Routes/ClaimsRoutes')
const claimItemsRoutes = require('./Routes/ClaimItemsRoutes')
const app = require('express')()
const bodyParser = require('body-parser')
var connectdb = require('./Helper/dbconnect')

app.use(bodyParser.raw());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.use('/cancelpaypend',paypendroutes)
app.use('/token',tokenroutes)
app.use('/enrollment',enrollmentroutes)
app.use('/claim',claimroutes)
app.use('/client',clientroutes)
app.use('/fpt',financeProcessingTypeRoutes)
app.use('/company',companyRoutes)
app.use('/subcompany',subcompanyRoutes)
app.use('/employee',employeeRoutes)
app.use('/claimdev',claimDevRoutes)
app.use('/tes',tesRoutes)
app.use('/user',userRoutes)
app.use('/roles',rolesRoutes)
app.use('/userroles',userRolesRoutes)
app.use('/clinic',clinicRoutes)
app.use('/claims',claimsRoutes)
app.use('/claimitems',claimItemsRoutes)



connectdb.authenticate()
.then(() => {
  console.log('Connection to database established successfully')
})
.catch(err => {
  console.log('Unable to connect to database: ', err)
})

app.get('/', function (req, res) {
  res.send('hello world 234')
})


app.listen(3031)