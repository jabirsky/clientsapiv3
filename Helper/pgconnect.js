const Sequelize = require('sequelize')

const connectdb = new Sequelize('eClaimsUAT', 'SA', 'FHC@ID2017', {
    dialect: 'mssql',
    host: '172.17.221.183',
    dialectOptions: {
      instanceName: 'FHNIDUAT',
      requestTimeout: 30000, // timeout = 30 seconds
      trustedConnection: true
    }
  })

module.exports =  connectdb  ;