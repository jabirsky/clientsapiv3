var connectdb = require('../Helper/dbconnect')

var enrollment = (req, res) => {

    let jsonstring = JSON.stringify(req.body)
    console.log(req.body)
    console.log(req.body.EndPolicyDate)
    var EnrollmentCode = req.body.EnrollmentCode ? req.body.EnrollmentCode:''
    var RecordType = req.body.RecordType ? req.body.RecordType:''
    var Relation = req.body.Relation ? req.body.Relation:''
    connectdb.query(`sp_Enrollment_API
    @Enrollment_Code= '${EnrollmentCode}',
    @Record_Type= '${RecordType}',
    @client_code= '${req.body.ClientCode?req.body.ClientCode:''}',
    @company_code= '${req.body.CompanyCode?req.body.CompanyCode:''}',
    @policy_number= '${req.body.PolicyNumber?req.body.PolicyNumber:''}',
    @EmployeeID= '${req.body.EmployeeId?req.body.EmployeeId:''}',
    @Principal_ID= '${req.body.PrincipalId?req.body.PrincipalId:''}',
    @Relation= '${Relation}',
    @Name= '${req.body.MemberName?req.body.MemberName:''}',
    @IsVIPEmployee= '${req.body.IsVipEmployee?req.body.IsVipEmployee:''}',
    @Gender= '${req.body.Gender?req.body.Gender:''}',
    @DOB= '${req.body.Dob?req.body.Dob:''}',
    @PlanCode= '${req.body.PlanCode?req.body.PlanCode:''}',
    @Email= '${req.body.Email?req.body.Email:''}',
    @Address= '${req.body.Address?req.body.Address:''}',
    @BankID= '${req.body.BankId?req.body.BankId:''}',
    @Account_Number= '${req.body.AccountNumber?req.body.AccountNumber:''}',
    @AcctName= '${req.body.AccountName?req.body.AccountName:''}',
    @JoinDt= '${req.body.JoinDate?req.body.JoinDate:''}',
    @Start_Policy_Date= '${req.body.StartPolicyDate?req.body.StartPolicyDate:''}',
    @End_Policy_Date= '${req.body.EndPolicyDate?req.body.EndPolicyDate:''}',
    @Effective_Date= '${req.body.EffectiveDate?req.body.EffectiveDate:''}',
    @suspend_status= '${req.body.PolicyStatus?req.body.PolicyStatus:''}',
    @SubCompanyId= '${req.body.SubCompanyId?req.body.SubCompanyId:''}',
    @maritalstatus= '${req.body.MaritalStatus?req.body.MaritalStatus:''}',
    @telephone_mobile= '${req.body.MobileNumber?req.body.MobileNumber:''}',
    @pre_existing= '${req.body.PreExisting?req.body.PreExisting:''}',
    @Remarks= '${req.body.Remarks?req.body.Remarks:''}',
    @salary_as_product= '${req.body.SalaryAsProduct?req.body.SalaryAsProduct:''}',
    @salary= '${req.body.Salary?req.body.Salary:''}',
    @SchemeId= '${req.body.ProductId?req.body.ProductId:''}',
    @grade= '${req.body.Grade?req.body.Grade:''}',
    @additional_1= '${req.body.Additional1?req.body.Additional1:''}',
    @additional_2= '${req.body.Additional2?req.body.Additional2:''}',
    @additional_3= '${req.body.Additional3?req.body.Additional3:''}',
    @additional_4= '${req.body.Additional4?req.body.Additional4:''}',
    @code_option= '${req.body.CodeOption}'`, { type: connectdb.QueryTypes.SELECT })
        .then(
            result => {
                res.send(result[0]);
            }
        ).catch(
            err => {
                console.log(err);
                res.send({ResponseCode:99, ResponseDescription: 'Unknown Error, Please Contact Administrator'});
            }
        )
}

module.exports = { enrollment }