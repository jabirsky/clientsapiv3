var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var SubCompanyCtrl = require('../Controller/SubCompany');
router.get("/subcompany", SubCompanyCtrl.SubCompanyAll);
module.exports = router
