var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var claimctrl = require('../Controller/Claim');
router.post("/claim",authentication.auth,claimctrl.claim);
router.post("",authentication.auth,claimctrl.claim);
module.exports = router