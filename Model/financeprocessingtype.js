var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var   ClientMod = require('./client')

// var FinanceProcessingType = connectdb.define('FinanceProcessingType',{
//     id:{type:Sequelize.INTEGER(10), allowNull: false, primaryKey: true, autoIncrement: true},
//     Description:{type:Sequelize.STRING, allowNull: false},
//     created_by:{type:Sequelize.STRING},
//     createddate:{type:Sequelize.DATE},
// },
// {
//     freezeTableName: true,
//     timestamps: false
// })

class FinanceProcessingType extends Sequelize.Model {}
FinanceProcessingType.init({
    id:{type:Sequelize.INTEGER(10), allowNull: false, primaryKey: true, autoIncrement: true},
    Description:{type:Sequelize.STRING, allowNull: false},
    created_by:{type:Sequelize.STRING},
    createddate:{type:Sequelize.DATE}
}, { 
    sequelize:connectdb, 
    modelName: 'FinanceProcessingType' ,
    freezeTableName: true,
    timestamps: false
});

//FinanceProcessingType.hasMany(ClientMod, { foreignKey: 'FinanceProcessingTypeId', as:FinanceProcessingType })
module.exports =  FinanceProcessingType 