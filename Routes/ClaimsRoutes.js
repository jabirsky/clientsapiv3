var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var ClaimsCtrl = require('../Controller/Claims');
router.get("/claims", ClaimsCtrl.claimAll);
module.exports = router
