var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var employeeCtrl = require('../Controller/employee');
router.get("/employee", employeeCtrl.EmployeeAll);
module.exports = router