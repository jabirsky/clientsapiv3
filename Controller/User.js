var userMod = require('../Model/User')
var userRolesMod = require('../Model/UserRoles')
var RolesMod = require('../Model/Roles')
var userClientMod = require('../Model/UserClient')
var ClientMod = require('../Model/client')
var ClinicMod = require('../Model/Clinic')
var CompanyMod = require('../Model/company')
var UsermatrixMod = require('../Model/UserMatrix')


var UserAll = async(req, res) => {
    try{
        console.log('panggil UserAll');
        var getUser = await userMod.findAll(
            {
                include :
                [{
                    model: userRolesMod,
                    as: 'UserRoles',
                    attributes:['RoleId'],
                    include: 
                    {
                        model: RolesMod,
                        as : 'Roles',
                        attributes:['Name']
                    }
                },{
                    model:  ClientMod,
                    as: 'Clients',
                    attributes:['ClientID','ClientCode','ClientName']
                },{
                    model:  CompanyMod,
                    as: 'Companies',
                    attributes:['CoyID','CoyName']
                },{
                    model:  ClinicMod
                },{
                    model:  UsermatrixMod,
                    required : false,
                    where: { IsDelete: false }
                }]
            }
        );
        res.send(getUser);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}

var UserByUserName = async(req, res) => {
    try{
        console.log(req);
        var getUser = await userMod.findAll(
            {
                where: {
                    UserName : req.params.username
                  }
            }
        );
        res.send(getUser);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}


module.exports =  { UserAll,UserByUserName }