var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var claimdevCtrl = require('../Controller/claimdev');
router.get("/claimdev", claimdevCtrl.ClaimAll);
module.exports = router