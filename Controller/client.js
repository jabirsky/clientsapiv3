var clientMod = require('../Model/client')
var financeprocessingtypeMod = require('../Model/financeprocessingtype')
var companyMod = require('../Model/company')


var ClientAll = async(req, res) => {
    try{
        var getClient = await clientMod.findAll(
            {
            attributes: ['ClientID','CLientName'],
            include: [{
            model: financeprocessingtypeMod,
            attributes: ['Description']
          },{
            model: companyMod  
          }]}
        );
        res.send(getClient);
    }
    catch(e)
    {
        res.send(e);
    }
}

var InsertClient = async(req, res) => {
    try{
        console.log(req.body)
        var newclient = await clientMod.create({ClientCode:req.body.ClientCode,ClientName: req.body.ClientName });
        res.send(newclient);
    }
    catch(e)
    {
        console.log(e)
        res.send(e);
    }
}

var UpdateClient = async(req, res) => {
    try{
        console.log(req.body)
        var newclient = await clientMod.update({ClientCode:req.body.ClientCode},{ where: { ClientID: 1295 } });
        res.send(newclient);
    }
    catch(e)
    {
        console.log(e)
        res.send(e);
    }
}

var DeleteClient = async(req, res) => {
    try{
        console.log(req.body)
        var deleteclient = await clientMod.destroy({ where: { ClientID: 1303}});
        console.log(deleteclient)
        res.send(1)
    }
    catch(e)
    {
        console.log(e)
        res.send(e);
    }
}


module.exports =  { ClientAll, InsertClient, UpdateClient, DeleteClient }