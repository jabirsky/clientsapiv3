var express = require('express');
var router = express.Router();
var authentication = require('../Helper/AuthMiddleware')
var tesCtrl = require('../Controller/tes');
router.get("/tes", tesCtrl.TestAll);
module.exports = router