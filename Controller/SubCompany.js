var clientMod = require('../Model/client')
var financeprocessingtypeMod = require('../Model/financeprocessingtype')
var subcompanyMod = require('../Model/SubCompany')
var companyMod = require('../Model/company')

var SubCompanyAll = async(req, res) => {
    try{
        console.log('panggil sub companyall');
        var getSubCompany = await subcompanyMod.findAll(
            {include : {
                model : companyMod
            }}
        );
        res.send(getSubCompany);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}


module.exports =  { SubCompanyAll }