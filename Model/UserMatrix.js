var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var ClientMod =require('../Model/client')

class UserMatrix extends Sequelize.Model {}
UserMatrix.init({
  Id: {type:Sequelize.INTEGER,primaryKey:true ,allowNull:false},
  UserId: {type:Sequelize.STRING,allowNull:false},
  ClientConfig: Sequelize.BOOLEAN,
  CompanyConfig: Sequelize.BOOLEAN,
  SubCompanyConfig: Sequelize.BOOLEAN,
  IsDelete : Sequelize.BOOLEAN
}, { 
    sequelize:connectdb, 
    modelName: 'UserMatrix' ,
    freezeTableName: true,
    timestamps: false
});

module.exports =  UserMatrix 