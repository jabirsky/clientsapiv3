var claimsItemMod = require('../Model/ClaimItems')
var sequelize = require('../Helper/dbconnect')
const Sequelize = require('sequelize')


var claimItemsAll = async(req, res) => {
    try{
        console.log('panggil claimitems');
        var getClaimItem = await claimsItemMod.findAll(
            { 
                offset: 10, 
                limit: 1000 
            }
        );
        res.send(getClaimItem);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}

var claimItemsByClaim= async(req, res) => {
    try{
        console.log('panggil claimitems');
        var getClaimItem = await claimsItemMod.findAll(
            {
                attributes: ['ClaimId',[sequelize.fn('sum', sequelize.col('ApprovedAmount')), 'sumApprovedAmount'],[sequelize.fn('sum', sequelize.col('AmountPerDay')), 'sumAmountPerDay']],
                group : ['ClaimId'],
                raw: true,
                where :{
                    ClaimId : req.params.ClaimId
                }
            }   
        )
        res.send(getClaimItem);
    }
    catch(e)
    {
        console.log(e);
        res.send(e);
    }
}


module.exports =  { claimItemsAll, claimItemsByClaim }