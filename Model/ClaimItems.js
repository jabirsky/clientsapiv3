var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')

class ClaimItem extends Sequelize.Model {}
ClaimItem.init({
  ClaimId: {type:Sequelize.STRING,primaryKey: true,allowNull:false},
  ItemId: Sequelize.INTEGER,
  AmountPerDay : Sequelize.DECIMAL(18,2),
  ApprovedAmount : Sequelize.DECIMAL(18,2),
  PatientPaid : Sequelize.DECIMAL(18,2),
  ExcessCharge : Sequelize.DECIMAL(18,2),
  UnpaidAmount : Sequelize.DECIMAL(18,2)
}, { 
    sequelize:connectdb, 
    modelName: 'tb_ClaimItems' ,
    freezeTableName: true,
    timestamps: false
});

module.exports =  ClaimItem 