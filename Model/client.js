var connectdb = require('../Helper/dbconnect')
const Sequelize = require('sequelize')
var FinanceProcessingTypeMod =require('../Model/financeprocessingtype')
var CompanyMod =require('../Model/company')
var SubCompanyMod =require('../Model/SubCompany')

class client extends Sequelize.Model {}
client.init({
  ClientID: {type:Sequelize.INTEGER(10),primaryKey: true, autoIncrement: true},
  ClientCode: Sequelize.STRING,
  ClientName: Sequelize.STRING,
  FinanceProcessingTypeId: {
    type: Sequelize.INTEGER,
    field: 'FinanceProcessingType',
    references: {
        // This is a reference to another model
        model: FinanceProcessingTypeMod,
        
        // This is the column name of the referenced model
        key: 'id',
      }
  },
  ClientAddress:{type:Sequelize.STRING}
}, { 
    sequelize:connectdb, 
    modelName: 'tb_client' ,
    freezeTableName: true,
    timestamps: false
});

client.belongsTo(FinanceProcessingTypeMod)
FinanceProcessingTypeMod.hasMany(client)
client.hasMany(CompanyMod,{ foreignKey: 'ClientID' })
CompanyMod.belongsTo(client,{ foreignKey: 'ClientID' })

module.exports =  client 



// var client = connectdb.define('tb_client',{
//     ClientID:{type:Sequelize.INTEGER(10), allowNull: false, primaryKey: true, autoIncrement: true},
//     ClientCode:{type:Sequelize.STRING, allowNull: false},
//     ClientName:{type:Sequelize.STRING, allowNull: false},
//     // ClientAddress:{type:Sequelize.STRING},
//     // ContactPerson:{type:Sequelize.STRING},
//     // Phone:{type:Sequelize.STRING},
//     // EmailID:{type:Sequelize.STRING},
//     // CreatedBy:{type:Sequelize.STRING, allowNull: false},
//     // CreatedDate:{type:Sequelize.DATE, allowNull: false},
//     // UpdatedBy:{type:Sequelize.STRING},
//     // UpdatedDate:{type:Sequelize.DATE},
//     // Status:{type:Sequelize.STRING},
//     // BankID:{type:Sequelize.STRING},
//     // BranchID:{type:Sequelize.STRING},
//     // AccountName:{type:Sequelize.STRING},
//     // AccountNumber:{type:Sequelize.STRING},
//     // BankAddress:{type:Sequelize.STRING},
//     // CtExcess:{type:Sequelize.STRING},
//     // reactive_same_start_polis:{type:Sequelize.STRING},
//     // ClientType:{type:Sequelize.STRING},
//     // ExcessValidation:{type:Sequelize.BOOLEAN},
//     // excessagingday:{type:Sequelize.INTEGER},
//     // JSON_GEOLOCATION:{type:Sequelize.STRING},
//     // paymentprocess:{type:Sequelize.INTEGER},
//     // AutoAddFlag:{type:Sequelize.BOOLEAN},
//     // IsUsingPanel:{type:Sequelize.BOOLEAN},
//     // PublicClaims:{type:Sequelize.BOOLEAN},
//     // IsFinanCeProcessing:{type:Sequelize.BOOLEAN},
//     // ClientCustomCard:{type:Sequelize.BOOLEAN},
//     FinanceProcessingType:{type:Sequelize.INTEGER}
//     // auto_provider_exclussion:{type:Sequelize.BOOLEAN}
// },
// {
//     freezeTableName: true,
//     timestamps: false
// })
