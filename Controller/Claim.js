var connectdb = require('../Helper/dbconnect')

// var claim = (req, res) => {
//     console.log(req.body)
//     console.log(req.body.Detail[2].ItemCode)
//     let payorId = req.body.PayorId ? req.body.PayorId : ''
//     let corporateId = req.body.CorporateId ? req.body.CorporateId : ''
//     connectdb.query(`sp_insert_reimbursementclaim 
//     @PayorId='${payorId} '
//     ,@CorporateId='${corporateId}'
//     ,@PolicyNo='${req.body.PolicyNo}'
//     ,@MembId='${req.body.MembId}'
//     ,@MembNm='${req.body.MembNm}'
//     ,@MembSts='${req.body.MembSts}'
//     ,@ClaimType='${req.body.ClaimType}'
//     ,@ClaimSts='${req.body.ClaimSts}'
//     ,@ClaimRef='${req.body.ClaimRef}'
//     ,@RefNo='${req.body.RefNo? req.body.RefNo:''}'
//     ,@AdmedikaClaimId='${req.body.AdmedikaClaimId}'
//     ,@ProviderId='${req.body.ProviderId}'
//     ,@AdmissionDt='${req.body.AdmissionDt}'
//     ,@DischargeDt='${req.body.DischargeDt}'
//     ,@Los='${req.body.Los}'
//     ,@Coverage='${req.body.Coverage}'
//     ,@PlainId='${req.body.PlainId}'
//     ,@DiagId='${req.body.DiagId}'
//     ,@DiagNm='${req.body.DiagNm}'
//     ,@TotAmtIncurred='${req.body.TotAmtIncurred}'
//     ,@TotAmtApprove='${req.body.TotAmtApprove}'
//     ,@TotAmtNotApprove='${req.body.TotAmtNotApprove}'
//     ,@TotEocPaid='${req.body.TotEocPaid}'
//     ,@Remark='${req.body.Remark}'
//     ,@OtherDiag='${req.body.OtherDiag}'
//     ,@ApproveDt='${req.body.ApproveDt}'
//     ,@ApproveBy='${req.body.ApproveBy}'
//     ,@Dor='${req.body.Dor}'
//     ,@HospitalInvNo='${req.body.HospitalInvNo}'`, { type: connectdb.QueryTypes.SELECT })
//         .then(
//             result => {
//                 res.send(result[0]);
//             }
//         ).catch(
//             err => {
//                 res.send(err);
//             }
//         )
//     res.send('sip');
// }

var claim = (req, res) => {

    let payorId = req.body.PayorId ? req.body.PayorId : ''
    let corporateId = req.body.CorporateId ? req.body.CorporateId : ''
    let jsonstring = JSON.stringify(req.body)
    console.log(jsonstring)
    connectdb.query(`InsertClaimApi 
    @ClientCode='${req.body.ClientCode ? req.body.ClientCode : ''} '
    ,@JsonRequest='${jsonstring}'`, { type: connectdb.QueryTypes.SELECT })
        .then(
            result => {
                res.send(result[0]);
            }
        ).catch(
            err => {
                res.send({ResponseCode:99, ResponseDescription: 'Unknown Error, Please Contact Administrator'});
            }
        )
}

module.exports = { claim }